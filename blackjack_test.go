package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCardclass(t *testing.T) {
	testCard := new(Card)
	testCard.Init("Ace", "Spades")
	assert := assert.New(t)
	assert.Equal(testCard.Describe(), "Ace of Spades")
	assert.Equal(testCard.Value(), 11)
	testCard.Init("King", "Clubs")
	assert.Equal(testCard.Describe(), "King of Clubs")
	assert.Equal(testCard.Value(), 10)
	testCard.Init("8", "Hearts")
	assert.Equal(testCard.Describe(), "8 of Hearts")
	assert.Equal(testCard.Value(), 8)
}

func TestNewdeck(t *testing.T) {
	testDeck := new(Deck)
	testDeck.Init()
	assert := assert.New(t)
	topCard := testDeck.cards[0]
	assert.Equal(topCard.Describe(), "Ace of Hearts")
	assert.Equal(len(testDeck.cards), 52)
}

func TestShuffledeck(t *testing.T) {
	testDeck := new(Deck)
	testDeck.Init()
	assert := assert.New(t)
	testDeck.Shuffle()
	topCard := testDeck.cards[0]
	assert.NotEqual(topCard.Describe(), "Ace of Hearts")
}

func TestDeal(t *testing.T) {
	testDeck := new(Deck)
	testDeck.Init()
	assert := assert.New(t)
	dealtCard := testDeck.Deal()
	assert.Equal(dealtCard.Describe(), "Ace of Hearts")
	assert.Equal(len(testDeck.cards), 51)
	secondCard := testDeck.Deal()
	assert.NotEqual(secondCard.Describe(), "Ace of Hearts")
	assert.Equal(len(testDeck.cards), 50)
}

func TestHand(t *testing.T) {
	testDeck := new(Deck)
	testDeck.Init()
	assert := assert.New(t)
	hand := new(Hand)
	hand.Init("Gobbler")
	hand.hit(testDeck)
	testCard := hand.cards[0]
	assert.Equal(testCard.Describe(), "Ace of Hearts")
	assert.Equal(hand.total(), 11)
	assert.Equal(len(testDeck.cards), 51)
	hand.hit(testDeck)
	assert.Equal(hand.total(), 13)
	assert.Equal(len(testDeck.cards), 50)
	assert.Equal(hand.show(), "Gobbler has:\nAce of Hearts\n2 of Hearts\nTotal: 13")
	hand.Init("Oggy")
	assert.Equal(len(hand.cards), 0)
}

func TestAceLogic(t *testing.T) {
	hand := new(Hand)
	hand.Init("Sausage")
	card := new(Card)
	card.Init("Ace", "Spades")
	hand.cards = append(hand.cards, card)
	card = new(Card)
	card.Init("King", "Hearts")
	hand.cards = append(hand.cards, card)
	assert.Equal(t, hand.total(), 21)
	card = new(Card)
	card.Init("Jack", "Diamonds")
	hand.cards = append(hand.cards, card)
	assert.Equal(t, hand.total(), 21)
	hand = new(Hand)
	hand.Init("Wogga")
	card = new(Card)
	card.Init("Ace", "Spades")
	hand.cards = append(hand.cards, card)
	card = new(Card)
	card.Init("9", "Diamonds")
	hand.cards = append(hand.cards, card)
	assert.Equal(t, hand.total(), 20)
	card = new(Card)
	card.Init("8", "Clubs")
	hand.cards = append(hand.cards, card)
	assert.Equal(t, hand.total(), 18)
}

func Test_main(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			main()
		})
	}
}
