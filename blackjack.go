package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

// Card : The Card type which has a value and a suit
type Card struct {
	cardValue string
	suit      string
}

// Init : Initialise the card with a value and suit
func (card *Card) Init(cardValue string, suit string) {
	card.cardValue = cardValue
	card.suit = suit
}

// Describe : Print the card value and suit to the console
func (card *Card) Describe() string {
	return fmt.Sprintf("%v of %s", card.cardValue, card.suit)
}

// Value : work out the integer value of the card
func (card *Card) Value() int {
	cardvalues := map[string]int{"Ace": 11, "Jack": 10, "Queen": 10, "King": 10, "Ace(Low)": 1}
	returnint := 0
	stringvalue, ok := cardvalues[card.cardValue]
	if ok {
		returnvalue := stringvalue
		returnint = returnvalue
	} else {
		returnvalue, err := strconv.Atoi(card.cardValue)
		if err == nil {
			returnint = returnvalue
		}
	}
	return returnint
}

// LowerAce : Change the value of an ace card from 11 to 1
func (card *Card) LowerAce() {
	if card.cardValue == "Ace" {
		card.cardValue = "Ace(Low)"
	}
}

// Deck : The Deck type = contains a slice array of cards
type Deck struct {
	cards []*Card
}

// Init : Initialise the deck with all 52 cards
func (deck *Deck) Init() {
	suits := [4]string{"Hearts", "Diamonds", "Clubs", "Spades"}
	cardValues := [13]string{"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"}
	for _, suit := range suits {
		for _, value := range cardValues {
			newCard := new(Card)
			newCard.Init(value, suit)
			deck.cards = append(deck.cards, newCard)
		}
	}

}

// Shuffle : shuffle the cards in the deck 4 times
func (deck *Deck) Shuffle() {
	rand.Seed(time.Now().UnixNano())
	for s := 1; s <= 5; s++ {
		rand.Shuffle(len(deck.cards), func(i, j int) { deck.cards[i], deck.cards[j] = deck.cards[j], deck.cards[i] })
	}
}

// Deal : take the topmost card from the deck and return it
func (deck *Deck) Deal() *Card {
	dealtCard := deck.cards[0]
	deck.cards = deck.cards[:copy(deck.cards[0:], deck.cards[1:])]
	return dealtCard
}

// Hand : the hand type - a slice array of cards
type Hand struct {
	cards []*Card
	owner string
}

// Init : initialise the hand type
func (hand *Hand) Init(owner string) {
	hand.owner = owner
	hand.cards = nil
}

// hit : add the returned card from deck.deal to the hand
func (hand *Hand) hit(deck *Deck) {
	hand.cards = append(hand.cards, deck.Deal())
}

// total : output the numeric total of the hand, including lowering any aces if encessary
func (hand *Hand) total() int {
	titalNonAces := 0
	total := 0
	for _, card := range hand.cards {
		if card.Value() != 11 {
			titalNonAces += card.Value()
		}
	}
	if titalNonAces > 10 {
		for _, card := range hand.cards {
			if card.Value() == 11 {
				card.LowerAce()
			}
		}
	}
	for _, card := range hand.cards {
		total += card.Value()
	}
	return total
}

// show ; print the cards in the hand to the console
func (hand *Hand) show() string {
	showhand := fmt.Sprintf("%v has:\n", hand.owner)
	for _, card := range hand.cards {
		showhand += card.Describe()
		showhand += "\n"
	}
	showhand += "Total: "
	showhand += strconv.Itoa(hand.total())
	return showhand
}

// Game : The game type
type Game struct {
	deck Deck
}

// Init : Initialise the game type
func (game *Game) Init() {
	game.deck.Init()
}

// hitOrStick : prompt the payer to hit or stick and return it
func (game *Game) hitOrStick() int {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Hit H to hit or S to stick: ")
	choice, _ := reader.ReadString('\n')
	choice = strings.ToLower(strings.Replace(choice, "\n", "", -1))
	if choice == "s" {
		return 0
	}
	return 1
}

// play`round : play a round of Blackjack
func (game *Game) playRound() {
	playerscore := 0
	dealerscore := 0
	deck := new(Deck)
	deck.Init()
	deck.Shuffle()
	playerhand := new(Hand)
	dealerhand := new(Hand)
	playerhand.Init("Player")
	dealerhand.Init("Dealer")
	for i := 1; i <= 2; i++ {
		playerhand.hit(deck)
	}
	dealerhand.hit(deck)
	fmt.Println(dealerhand.show())
	fmt.Println(playerhand.show())
	hit := game.hitOrStick()
	for playerhand.total() < 21 && hit == 1 {
		playerhand.hit(deck)
		time.Sleep(1 * time.Second)
		fmt.Println(playerhand.show())
		if playerhand.total() > 21 {
			fmt.Println("Player busts!")
			playerscore = -1
			break
		}
		hit = game.hitOrStick()
	}
	if playerscore > -1 {
		playerscore = playerhand.total()
	}
	time.Sleep(1 * time.Second)
	fmt.Println("Dealer to play")
	for dealerhand.total() < 17 {
		dealerhand.hit(deck)
		time.Sleep(1 * time.Second)
		fmt.Println(dealerhand.show())
		if dealerhand.total() > 21 {
			fmt.Println("Dealer busts!")
			dealerscore = -1
			break
		}
	}
	if dealerscore > -1 {
		dealerscore = dealerhand.total()
	}
	fmt.Println()
	if playerscore > dealerscore {
		fmt.Println("Player Wins!!")
	} else if playerscore < dealerscore {
		fmt.Println("Dealer Wins!!")
	} else if playerscore == dealerscore {
		fmt.Println("A Tie!")
	}

}

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Jimbo's BlackJack.  Press Y for a game  ")
	choice, _ := reader.ReadString('\n')
	choice = strings.Replace(choice, "\n", "", -1)
	for choice == "y" {
		game := new(Game)
		game.playRound()
		fmt.Print("Play again?   Y/N: ")
		choice, _ := reader.ReadString('\n')
		choice = strings.Replace(choice, "\n", "", -1)
		if choice != "y" {
			break
		}
	}
}
